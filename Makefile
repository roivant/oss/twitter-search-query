# Makefile for twitter-search-query

# Print usage of main targets when user types "make" or "make help"
help:
	@echo -e "Available commands:\
	\n   setup: Initialize development environment\
	\n   compile_dependencies: Compile python dependencies \
	\n   dependencies: Install python dependencies \
	\n   setup_bigquery: Create BigQuery objects \
	"

PYTHON_VERSION=3.8.7
REPO_NAME=twitter-search-query

# Sets up a virtualenv that is managed by pyenv
.PHONY: pyenv
pyenv:
ifneq (${CI}, true)
	pyenv install -s ${PYTHON_VERSION}
	@[ ! -e ~/.pyenv/versions/${REPO_NAME} ] && pyenv virtualenv ${PYTHON_VERSION} ${REPO_NAME} || true
	pyenv local ${REPO_NAME}
endif


# Install project dependencies
.PHONY: dependencies
dependencies:
	pip install -U "pip<21.3" temple
	pip install -r requirements.txt
	pip check


# Compile requirements files
export CUSTOM_COMPILE_COMMAND=make compile_dependencies
.PHONY: compile_dependencies
compile_dependencies:
	pip-compile requirements.in --no-emit-index-url


# Setup a development environment
.PHONY: setup
setup: pyenv dependencies


# Create BigQuery objects
.PHONY: setup_bigquery
setup_bigquery:
	./scripts/setup_bigquery.sh

