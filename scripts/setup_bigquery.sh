# Set project context
gcloud config set project $BQ_PROJECT

# Create dataset
bq --location=$GCP_LOCATION mk --dataset $BQ_PROJECT:$BQ_DATASET

# Create tables and views
bq query --use_legacy_sql=false --dataset_id=$BQ_DATASET < twitter/schemas/setup_bigquery.sql
