import datetime
from typing import Tuple

TIMESTAMP_FMT = "%Y-%m-%d %H:%M:%S"
DATE_FMT = "%Y-%m-%d"


def get_utc_now() -> datetime.datetime.utcnow:
    return datetime.datetime.utcnow()


def get_utc_now_timestamp() -> str:
    return get_utc_now().strftime(TIMESTAMP_FMT)


def get_utc_now_date() -> str:
    return get_utc_now().strftime(DATE_FMT)


def set_lookback(days: int) -> Tuple[str, str]:
    """Return ``since`` and ``until`` date strings representing lookback window.

    Args:
        days: Qty of days to look back.

    Returns:
        Tuple containing date strings representing lookback window (e.g. (since, until)).
    """
    since = (get_utc_now() - datetime.timedelta(days=days)).strftime(DATE_FMT)
    until = get_utc_now_date()
    return since, until
