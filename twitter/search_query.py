from typing import List


def from_users(criteria_value: List[str]) -> str:
    """Find Tweets FROM users"""
    return f"({' '.join([f'from:{v}' for v in criteria_value])})"


def include_words(criteria_value: List[str]) -> str:
    """Find Tweets containing ALL words"""
    return " ".join([v for v in criteria_value])


def optional_words(criteria_value: List[str]) -> str:
    """Find Tweets containing ANY of the words"""
    assert len(criteria_value) >= 2, "not enough values"
    return f"({' OR '.join([v for v in criteria_value])})"


def exclude_words(criteria_value: List[str]) -> str:
    """Find Tweets NOT containing words"""
    return " ".join([f"-{v}" for v in criteria_value])


def include_phrases(criteria_value: List[str]) -> str:
    """Find Tweets containing exact phrase(s)"""
    return " ".join([f'"{v}"' for v in criteria_value])
