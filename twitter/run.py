import uuid
import logging
from typing import Dict, List

import auth
import derive
import load
import utils
import report as dummy_reports

import tweepy

logging.basicConfig(level=logging.INFO)
LOGGER = logging.getLogger(__name__)

# Remember to set Twitter credentials!
TWEEPY_API = auth.authenticate_api()
MAX_TWEETS_PER_PAGE = 100
LOOKBACK_DAYS = 7


def run_search_query(context: Dict):
    """Run search query and load results into BigQuery"""
    LOGGER.info(f"Running search query: {context} ...")
    status_count = 0
    for page in tweepy.Cursor(
        TWEEPY_API.search,
        q=context.get("search_query"),
        count=MAX_TWEETS_PER_PAGE,
        tweet_mode="extended",
        lang="en"
    ).pages():
        # Load one page at a time
        status_count += load.load_page(context, page)
    LOGGER.info(f"Done! Tweets collected: {status_count}")


def run_reports(reports: List[Dict]):
    """Individually run each report"""
    for r in reports:
        search_query = derive.derive_search_query(r.get("criteria"))
        since, until = utils.set_lookback(LOOKBACK_DAYS)
        context = {
            "load_uuid": str(uuid.uuid4()),
            "report_slug": r.get("report_slug"),
            "search_query": f"{search_query} since:{since} until:{until}"
        }
        run_search_query(context)


if __name__ == "__main__":
    run_reports(dummy_reports.REPORTS)
