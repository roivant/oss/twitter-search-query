import os
import logging
from typing import List, Dict

import scrub

import tweepy
from google.cloud import bigquery

BQ_CLIENT = bigquery.Client()
BQ_PROJECT = os.environ.get("BQ_PROJECT")
BQ_DATASET = os.environ.get("BQ_DATASET")
BQ_TABLE_ID_STATUS = f"{BQ_PROJECT}.{BQ_DATASET}.status"
BQ_TABLE_ID_USER = f"{BQ_PROJECT}.{BQ_DATASET}.user"

LOGGER = logging.getLogger(__name__)


def stream_into_table(bq_client: bigquery.Client, table_id: str, json_rows: List[Dict]):
    """Stream list of dictionaries into table"""
    errors = bq_client.insert_rows_json(table_id, json_rows)
    if len(errors) == 0:
        LOGGER.info(f"{len(json_rows)} rows added to BigQuery table: {table_id}")
    else:
        raise Exception(f"{len(errors)} streaming error(s) occurred: {errors}")


def load_page(context: Dict, page: List[tweepy.models.Status]) -> int:
    """Load entire page of Tweets and Users into BigQuery"""
    scrubbed_status_objs = list()
    scrubbed_user_objs = list()
    status_count = 0
    for status in page:

        # Prepare `status` record
        scrubbed_status = scrub.scrub_status(status)
        scrubbed_status.update(
            {
                "load_uuid": context.get("load_uuid"),
                "search_query": context.get("search_query"),
                "report_slug": context.get("report_slug"),
            }
        )
        scrubbed_status_objs.append(scrubbed_status)
        status_count += 1

        # Prepare `user` record
        scrubbed_user = scrub.scrub_user(status)
        scrubbed_user.update({"load_uuid": context.get("load_uuid")})
        scrubbed_user_objs.append(scrubbed_user)

    # Load `page` into BigQuery
    stream_into_table(BQ_CLIENT, BQ_TABLE_ID_STATUS, scrubbed_status_objs)
    stream_into_table(BQ_CLIENT, BQ_TABLE_ID_USER, scrubbed_user_objs)
    return status_count
