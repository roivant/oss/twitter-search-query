/*
    (1) status
        ~~~~~~
        Store ALL versions of each Tweet record.
*/
CREATE TABLE IF NOT EXISTS status
(
  status_inserted_at TIMESTAMP NOT NULL OPTIONS(description="Time status was inserted into table."),
  status_created_at TIMESTAMP NOT NULL OPTIONS(description="Time status was originally created in Twitter."),
  status_id INT64 NOT NULL,
  status_text STRING NOT NULL,
  in_reply_to_status_id INT64,
  in_reply_to_user_id INT64,
  in_reply_to_screen_name STRING,
  user_id INT64 NOT NULL,
  user_name STRING,
  user_screen_name STRING,
  status_place STRING OPTIONS(description="City/country associated with Tweet as reported by user"),
  quoted_status_id INT64,
  is_quote_status BOOL,
  retweeted_status_id INT64,
  quote_count INT64 OPTIONS(description="Qty of times Tweet has been quoted by Twitter users. NOTE: Premium API only."),
  reply_count INT64 OPTIONS(description="Qty of times Tweet has been replied to. NOTE: Premium API only."),
  retweet_count INT64 OPTIONS(description="Qty of times Tweet has been retweeted."),
  favorite_count INT64 OPTIONS(description="Qty of times Tweet has been liked by Twitter users."),
  status_hashtags STRING,
  status_urls STRING,
  status_user_mentions STRING,
  possibly_sensitive BOOL,
  status_language STRING,
  load_uuid STRING,
  report_slug STRING,
  search_query STRING,
)
PARTITION BY DATE(_PARTITIONTIME)
CLUSTER BY report_slug;


/*
    (2) status_latest
        ~~~~~~~~~~~~~
        Provide LATEST version of each Tweet record.
*/

CREATE VIEW IF NOT EXISTS status_latest AS
SELECT * EXCEPT (rownum)
FROM   (
    --filter to latest version of each unique record!
    SELECT   *,
             ROW_NUMBER() OVER (
                PARTITION BY status_id, report_slug  --primary key field(s)
                ORDER BY status_inserted_at DESC     --comparator
             ) AS rownum
    FROM     status
    )
WHERE  rownum = 1;


/*
    (3) user
        ~~~~
        Store ALL versions of each User record.
*/
CREATE TABLE IF NOT EXISTS user
(
  user_inserted_at TIMESTAMP NOT NULL OPTIONS(description="Time user was inserted into table."),
  user_created_at TIMESTAMP NOT NULL OPTIONS(description="Time user was originally created in Twitter."),
  user_id INT64 NOT NULL,
  user_name STRING,
  user_screen_name STRING NOT NULL,
  user_location STRING,
  user_url STRING,
  user_description STRING,
  verified BOOL OPTIONS(description="When true, indicates that the user has a verified account."),
  followers_count INT64 OPTIONS(description="Qty of followers this account currently has."),
  friends_count INT64 OPTIONS(description="Qty of users this account is following."),
  listed_count INT64 OPTIONS(description="Qty of public lists this user is a member of."),
  favourites_count INT64 OPTIONS(description="Qty of Tweets this user has liked."),
  statuses_count INT64 OPTIONS(description="Qty of Tweets (including retweets) issued by the user."),
  load_uuid STRING
)
PARTITION BY DATE(_PARTITIONTIME);


/*
    (4) user_latest
        ~~~~~~~~~~~
        Provide LATEST version of each User record.
*/
CREATE VIEW IF NOT EXISTS user_latest AS
SELECT * EXCEPT (rownum)
FROM   (
    --filter to latest version of each unique record!
    SELECT   *,
             ROW_NUMBER() OVER (
                PARTITION BY user_id            --primary key field(s)
                ORDER BY user_inserted_at DESC  --comparator
             ) AS rownum
    FROM     user
    )
WHERE  rownum = 1;
