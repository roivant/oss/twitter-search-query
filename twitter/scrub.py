import logging
from datetime import datetime

import tweepy
from dateutil.parser import parse

import utils

LOGGER = logging.getLogger(__name__)


def scrub_status(status: tweepy.models.Status) -> dict:
    """Prepare ``status`` record for insertion into BigQuery.

    Args:
        status: Status object (e.g. Tweet).

    Returns:
        Valid status record.
    """
    out = dict()
    out["status_inserted_at"] = utils.get_utc_now_timestamp()
    out["status_created_at"] = scrape_created_at(status.created_at)
    out["status_id"] = status.id_str
    if hasattr(status, "retweeted_status"):  # is Retweet?
        out["status_text"] = scrape_text(status.retweeted_status)
        out["status_hashtags"] = scrape_hashtags(status.retweeted_status)
        out["status_urls"] = scrape_urls(status.retweeted_status)
        out["status_user_mentions"] = scrape_user_mentions(status.retweeted_status)
        out["status_place"] = scrape_place(status.retweeted_status)
        out["retweeted_status_id"] = status.retweeted_status.id_str
    else:
        out["status_text"] = scrape_text(status)
        out["status_hashtags"] = scrape_hashtags(status)
        out["status_urls"] = scrape_urls(status)
        out["status_user_mentions"] = scrape_user_mentions(status)
        out["status_place"] = scrape_place(status)
        out["retweeted_status_id"] = None
    out["in_reply_to_status_id"] = status.in_reply_to_status_id_str
    out["in_reply_to_user_id"] = status.in_reply_to_user_id_str
    out["in_reply_to_screen_name"] = status.in_reply_to_screen_name
    out["user_id"] = status.user.id_str
    out["user_name"] = status.user.name
    out["user_screen_name"] = status.user.screen_name
    out["quoted_status_id"] = scrape_quoted_status_id(status)
    out["is_quote_status"] = status.is_quote_status
    out["quote_count"] = None  # Premium API only!
    out["reply_count"] = None  # Premium API only!
    out["retweet_count"] = status.retweet_count
    out["favorite_count"] = status.favorite_count
    out["possibly_sensitive"] = scrape_possibly_sensitive(status)
    out["status_language"] = status.lang
    return out


def scrub_user(status: tweepy.models.Status):
    """Prepare ``user`` record for insertion into BigQuery.

    Args:
        status: Status object (e.g. Tweet).

    Returns:
        Valid user record.
    """
    out = dict()
    out["user_inserted_at"] = utils.get_utc_now_timestamp()
    out["user_created_at"] = scrape_created_at(status.user.created_at)
    out["user_id"] = status.user.id_str
    out["user_name"] = status.user.name
    out["user_screen_name"] = status.user.screen_name
    out["user_location"] = status.user.location
    out["user_url"] = status.user.url
    out["user_description"] = status.user.description
    out["verified"] = status.user.verified
    out["followers_count"] = status.user.followers_count
    out["friends_count"] = status.user.friends_count
    out["listed_count"] = status.user.listed_count
    out["favourites_count"] = status.user.favourites_count
    out["statuses_count"] = status.user.statuses_count
    return out


def scrape_created_at(created_at: datetime) -> str:
    """Return ``created_at`` as BigQuery friendly timestamp.

    Args:
        created_at: Time status was created in Twitter-native format.

    Returns:
        Time status was created in BigQuery-friendly format.
    """
    return parse(str(created_at)).strftime(utils.TIMESTAMP_FMT)


def scrape_text(status: tweepy.models.Status) -> str:
    """Return ``text`` if exists.

    Supports statuses returned in either "extended" or "compatibility" modes.
    See https://docs.tweepy.org/en/stable/extended_tweets.html#examples

    Args:
        status: Status object (e.g. Tweet).

    Returns:
        Status text.
    """
    if hasattr(status, "extended_tweet"):
        return status.extended_tweet["full_text"]
    elif hasattr(status, "full_text"):
        return status.full_text
    elif hasattr(status, "text"):
        return status.text
    else:
        LOGGER.warning(f"Unable to scrape text from status_id: {status.id_str}")


def scrape_hashtags(status: tweepy.models.Status) -> list:
    """Return ``entities.hashtags`` array as a hash-delimited string::

        '#Foo #Bar'

    Args:
        status: Status object (e.g. Tweet).

    Returns:
        Any hashtags included in status.
    """
    if hasattr(status, "extended_tweet"):
        hashtags = status.extended_tweet["entities"]["hashtags"]
    else:
        hashtags = status.entities["hashtags"]

    if len(hashtags) > 0:
        hashtags_str = "#" + " #".join([h["text"] for h in hashtags])
        return hashtags_str


def scrape_urls(status: tweepy.models.Status):
    """Return ``entities.urls`` array as a pipe-delimited string::

        'https://google.com | https://yahoo.com'

    Args:
        status: Status object (e.g. Tweet).

    Returns:
        Any URLs included in status.
    """
    if hasattr(status, "extended_tweet"):
        urls = status.extended_tweet["entities"]["urls"]
    else:
        urls = status.entities["urls"]

    if len(urls) > 0:
        urls_str = " | ".join([u["url"] for u in urls])
        return urls_str


def scrape_user_mentions(status: tweepy.models.Status):
    """Return ``entities.user_mentions`` array as an at-delimited string::

        '@JohnSmith @JaneDoe'

    Args:
        status: Status object (e.g. Tweet).

    Returns:
        Any user mentions included in status.
    """
    if hasattr(status, "extended_tweet"):
        user_mentions = status.extended_tweet["entities"]["user_mentions"]
    else:
        user_mentions = status.entities["user_mentions"]

    if len(user_mentions) > 0:
        user_mentions_str = "@" + " @".join([u["screen_name"] for u in user_mentions])
        return user_mentions_str


def scrape_place(status: tweepy.models.Status):
    """Return ``place.full_name`` if exists as comma-delimited string::

        'New York City, US'

    Args:
        status: Status object (e.g. Tweet).

    Returns:
        Any locations included in status.
    """
    if status.place is not None:
        place_str = status.place.full_name
        return place_str


def scrape_quoted_status_id(status):
    """Return ``quoted_status_id`` if exists::

        '123456789'

    If a ``status`` quotes an existing ``status,`` then return its ``status_id``.

    Args:
        status: Status object (e.g. Tweet).

    Returns:
        ID of status that was originally quoted.
    """
    if hasattr(status, "quoted_status_id_str"):
        quoted_status_id_str = status.quoted_status_id_str
        return quoted_status_id_str


def scrape_possibly_sensitive(status):
    """Return ``possibly_sensitive`` if exists.

    If a status is flagged as possible sensitive, then return True.

    Args:
        status: Status object (e.g. Tweet).

    Returns:
        When true indicates status is possibly sensitive.
    """
    if hasattr(status, "possibly_sensitive"):
        return status.possibly_sensitive
