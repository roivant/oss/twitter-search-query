import os

import tweepy


def authenticate_api() -> tweepy.API:
    """Connect to Twitter API"""
    auth = tweepy.OAuthHandler(
        consumer_key=os.environ.get("TWITTER_CONSUMER_API_KEY"),
        consumer_secret=os.environ.get("TWITTER_CONSUMER_API_SECRET"),
    )
    auth.set_access_token(
        key=os.environ.get("TWITTER_ACCESS_TOKEN"),
        secret=os.environ.get("TWITTER_ACCESS_TOKEN_SECRET"),
    )
    api = tweepy.API(
        auth,
        # Auto-start exponential backoff loop for:
        #     420 - Rate limiting (used by Twitter API v1)
        #     429 - Rate limiting (used by Twitter API v1.1 and later)
        wait_on_rate_limit=True,
        wait_on_rate_limit_notify=True,
        # Auto-start fixed backoff loop (retry 3x times w/ 5sec delay) for:
        #     404 - Not Found
        #     401 - Unauthorized
        #     500 - Internal Server Error
        #     503 - Service Unavailable
        #     520 - Web Server Returned an Unknown Error
        retry_count=3,
        retry_delay=5,
        retry_errors={401, 404, 500, 503, 520},
    )
    return api
