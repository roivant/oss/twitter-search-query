REPORTS = [
    {
        "report_slug": "subway-delays",
        "criteria": [
            {
                "criteria_key": "from_users",
                "criteria_value": [
                    "NYCTSubway"
                ]
            },
            {
                "criteria_key": "optional_words",
                "criteria_value": [
                    "delay",
                    "delays",
                    "stopped"
                ]
            },
            {
                "criteria_key": "include_phrases",
                "criteria_value": [
                    "4/5"
                ]
            }
        ]
    },
    {
        "report_slug": "subway-news-northbound",
        "criteria": [
            {
                "criteria_key": "from_users",
                "criteria_value": [
                    "NYCTSubway"
                ]
            },
            {
                "criteria_key": "include_words",
                "criteria_value": [
                    "4",
                    "5",
                    "6"
                ]
            },
            {
                "criteria_key": "exclude_words",
                "criteria_value": [
                    "4/5"
                ]
            }
        ]
    }
]
