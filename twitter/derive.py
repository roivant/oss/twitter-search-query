from typing import Dict, List

import search_query as search_query_module


def derive_search_query(criteria: List[Dict]) -> str:
    """Derive search query from report"""
    search_query_functions = [
        f for f in dir(search_query_module) if not f.startswith("_") and f != "List"
    ]
    search_query = str()
    for criteria_item in criteria:
        criteria_key = criteria_item.get("criteria_key")
        criteria_value = criteria_item.get("criteria_value")
        if criteria_key not in search_query_functions:
            # criteria_key must match function name
            raise Exception(
                f"criteria_key: {criteria_key} must be one of: {search_query_functions}"
            )
        # append output of each function call
        search_query += " " + getattr(search_query_module, criteria_key)(criteria_value)
    return search_query.strip()
