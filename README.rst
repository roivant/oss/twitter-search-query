Twitter Search Query
=========================
Code sample demonstrating **core functions** used in Roivant's ``twitter`` DAG.

The **entry point** (``twitter/run.py``) derives a search query from each report and loads the results into BigQuery.

For more details, check out the **Medium post**: `Managing Complex Twitter Search Queries <https://medium.com/roivant-technology/managing-complex-twitter-search-queries-21bc5d2645c3>`_.


Prerequisites
-------------
* `Twitter API credentials <https://developer.twitter.com/en/docs/twitter-api/getting-started/getting-access-to-the-twitter-api>`_
* `Google Cloud Platform (GCP) account <https://cloud.google.com/>`_
* `GCP project <https://cloud.google.com/resource-manager/docs/creating-managing-projects>`_
* ``gcloud`` `command line tool <https://cloud.google.com/sdk/gcloud>`_
* ``pyenv`` & ``pyenv-virtualenv``


Usage
-----
Setup development environment::

    make setup


Authenticate with GCP::

    gcloud auth application-default login


Set project context::

    gcloud config set project <GCP_PROJECT>
    export BQ_PROJECT=<GCP_PROJECT>
    export BQ_DATASET=twitter
    export GCP_LOCATION=us-central1

Create dataset, tables, and views in BigQuery::

    make setup_bigquery

Set Twitter credentials::

    export TWITTER_CONSUMER_API_KEY=<secret-goes-here>
    export TWITTER_CONSUMER_API_SECRET=<secret-goes-here>
    export TWITTER_ACCESS_TOKEN=<secret-goes-here>
    export TWITTER_ACCESS_TOKEN_SECRET=<secret-goes-here>

Run each report & load results into BigQuery::

    python twitter/run.py

Now access the results!::

    select * from status_latest

